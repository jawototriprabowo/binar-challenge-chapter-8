
import './App.css';

import NewPlayer from './NewPlayer';
import UpdatePlayer from './UpdatePlayer';
import SearchPlayer from './SearchPlayer';

function App() {

  return (
    
    <div>
      <NewPlayer />
      <UpdatePlayer />
      <SearchPlayer />
    </div>
  );
}

export default App;
